# This script is run after building the packages/kernel/bootloader/etc

if [ -f "$BR2_QUARTUS_COPY_RBF" ]; then
    echo "Copying $BR2_QUARTUS_COPY_RBF to $BINARIES_DIR"
    cp "$BR2_QUARTUS_COPY_RBF" "$BINARIES_DIR"
fi