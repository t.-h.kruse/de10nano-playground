create_clock -period 20.000 -name sysclk -waveform {0.000 10.000} -add [get_ports clk_clk]

create_clock -period 20  [get_ports hps_io_hps_io_usb1_inst_CLK]

derive_pll_clocks -create_base_clock
