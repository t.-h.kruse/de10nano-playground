# DE10Nano playground project

This project excludes everything related to the FPGA currently.

## Prerequisites

- [buildroot (2023.02-rc3)](https://git.busybox.net/buildroot)
- [buildroot-eps (main)](https://git.gsi.de/t.-h.kruse/buildroot-eps)

Tested on Ubuntu 22.04.2 LTS

## Building

    cwd=project-root
    make -C [path-to-buildroot]/buildroot-repository BR2_EXTERNAL=[absolut-path-to]/buildroot-eps-repository O=$PWD/build BR2_DEFCONFIG=$PWD/buildroot/de10nano_nofpga_defconfig defconfig
    make menuconfig

Adjust `External options`-> `Project directory` (first entry) to the project's root.

    cd build
    make

The resulting `images/mmc.img` image can be flashed to the eMMC.

## Include FPGA bitstream (.rbf)

Uncomment line 7 in `buildroot/uboot-env.txt`

```bash
config_fpga=if load mmc 0:2 ${fpga_data} ${fpga_file}; then fpga load 0 ${fpga_data} ${filesize}; bridge enable; echo FPGA configured with [${fpga_file}]; else echo FPGA Configuration file [${fpga_file}] not found on mmc 0:2; fi
```

Add the .rbf file in the resulting mmc.img by adjusting `buildroot/genimage.cfg`

```
image boot.vfat {
    vfat {
        files = {
            "zImage",
            "socfpga.dtb",
            "socfpga.rbf"
        }
    }
    size = 16M
}
```

Specify the `.rbf` file via `make menuconfig` -> `External options` -> `Qaurtus Project` -> `Use RBF` (only available if `Use Quartus Project` is not selected)


## Initial setup

### Setup partitions

To use the unused storage of the MMC create a new partition via `fdisk /dev/mmcblk0` `n p` `239666` (may differ, EndLBA of last partition + 1) -> `Enter` -> `w`

Then format the new partition with ext4 and reboot

    mkfs.ext4 /dev/mmcblk0p4
    reboot

Now in `/mnt` there should be two folders `data` and `fat`.
`data` is a basic linux partition for user data and `fat` contains the boot files like linux kernel, fpga rbf and device tree.