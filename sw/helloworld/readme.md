# Example application to present remote debugging

**Prerequisites:**
- cross compiler (included in the buildroot build, so build buildroot first)
- cmake
- ninja-build
- libncursesw5
- libpython2.7

**Configure and build**

Open CMakePresets.json and adjust the paths according to your system

```bash
cmake --preset ninja-debug # configuration
cmake --build build/ninja-debug # building
```

**Debug**

Copy the application and the `main.cpp` file on the target.

Start the gdbserver

    gdbserver /dev/ttyGS0 helloworld

On the host start the cross gdb that comes with buildroot and specify the sysroot to use with gdb

    [project root dir]/build/host/bin/arm-none-linux-gnueabihf-gdb -ie "set sysroot [project root dir]/build/host" helloworld